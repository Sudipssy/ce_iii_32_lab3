# Creating Binary Search Tree By Linked List

class BinarySearchTree:
# Initialization
	def __init__(self):
		self._size = 0
		self._root = None #type bstnode
		
# Creting Node
	class _BSTNode:
		def __init__(self, key, value):
			self.key = key
			self.value = value
			self.left = None
			self.right = None

#Adding Node in BST
	def addNode(self, key, value):
		z = self._BSTNode(key, value)
		y =None
		x = self._root
		while (x != None):
			y = x
			if (key < x.key):
				x = x.left
			else:
				x = x.right
		if (y == None):
			self._root = z
		elif (z.key < y.key):
			y.left = z
		else:
			y.right = z
		self._size += 1
#Finding Smallest Key

	def findSmallestKey(self):
		nodes = []
		self._findSmallestKey(self._root, nodes)
		return nodes

	def _findSmallestKey(self, subtree, nodes):
		if(subtree):
			if(subtree.left == None):
				nodes.append(subtree.key)
			self._findSmallestKey(subtree.left, nodes)

#Finding Largest Key 

	def findLargestKey(self):
		nodes = []
		self._findLargestKey(self._root, nodes)
		return nodes

	def _findLargestKey(self, subtree, nodes):
		if(subtree):
			if(subtree.right == None):
				nodes.append(subtree.key)
			self._findLargestKey(subtree.right, nodes)
			
	def isEmpty(self):
		return self._size == 0

	def size(self):
		return self._size
		
# Inorder Traversal
	def inOrderWalk(self):
		nodes = []
		self._inOrderWalk(self._root, nodes)
		return nodes

	def _inOrderWalk(self, subtree, nodes):
		if(subtree):
			self._inOrderWalk(subtree.left, nodes)
			nodes.append(subtree.key)
			self._inOrderWalk(subtree.right, nodes)
			
# Preorder Travrsal
	def preOrderWalk(self):
		nodes = []
		self._preOrderWalk(self._root, nodes)
		return nodes

	def _preOrderWalk(self, subtree, nodes):
		if(subtree):
			nodes.append(subtree.key)
			self._preOrderWalk(subtree.left, nodes)
			self._preOrderWalk(subtree.right, nodes)

# Postorder Traversal
	def postOrderWalk(self):
		nodes = []
		self._postOrderWalk(self._root, nodes)
		return nodes

	def _postOrderWalk(self, subtree, nodes):
		if(subtree):
			self._preOrderWalk(subtree.left, nodes)
			self._preOrderWalk(subtree.right, nodes)
			nodes.append(subtree.key)
# Searching the Node
	def searchForNode(self, key):
		found = []
		self._searchForNode(self._root, key, found)
		return found

	def _searchForNode(self, subtree, key, found):
		if(subtree):
			if(key == subtree.key):
				found.append(1)
			elif(key < subtree.key):
				self._searchForNode(subtree.left, key, found)
			elif(key > subtree.key):
				self._searchForNode(subtree.right, key, found)




